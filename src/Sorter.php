<?php

/**
 * Class Sorter
 *
 */
class Sorter
{
    /**
     * Sorts the given unsorted itinerary elements. All elements should be an array with 'from' and 'to' keys containing origin and destination.
     *
     * @param array $input The unsorted itinerary elements.
     * @return array The sorted result.
     *
     * @throws Exception If the
     */
	public function sort(array $input) 
	{
		$result = $destinations = $origins = $listWithOriginKeys = $listWithDestinationKeys = [];

		foreach ($input as $key => $ticket) {
		    $destinations[] = $ticket['to'];
		    $listWithDestinationKeys[$ticket['to']] = $ticket;
		    $origins[] = $ticket['from'];
		    $listWithOriginKeys[$ticket['from']] = $ticket;
		}

		// the destination of the journey
		$destination = array_keys(array_diff($destinations, $origins));
		// the origin of the journey
		$origin      = array_keys(array_diff($origins, $destinations));

		//There should be only one destination and origin
		if(count($destination) !== 1 || count($origin) !== 1) {
		    throw new Exception('The given list is not valid!');
		}

		//the final result filled with origin
		$result[0] = $input[$origin[0]];
		//the final result filled with destination
		$result[count($input)-1] = $input[$destination[0]];

		//this is the key of the next element (in $listWithDestinationKeys) counting backwards from destination
		$keyOfNextDestination = $listWithDestinationKeys[$result[count($input)-1]['to']]['from'];

		//this is the key of the next element (in $listWithOriginKeys) counting forward from the origin
		$keyOfNextOrigin = $listWithDestinationKeys[$result[0]['to']]['to'];

		//We already changed the origin and the destination
		$changeCount = 2;
		$resultDestinationKey = count($input) - 1;

		for($i=1; $i < count($input) - 2; $i++) {
		    // In each iteration we fill one element from the direction of the destination, and one element from the direction of the origin
		    $resultDestinationKey--;
		    $result[$resultDestinationKey] = $listWithDestinationKeys[$keyOfNextDestination];
		    $keyOfNextDestination = $result[$resultDestinationKey]['from'];

		    $changeCount++;
		    if($changeCount > count($input)) {
			break;
		    }

		    $result[$i] = $listWithOriginKeys[$keyOfNextOrigin];
		    $keyOfNextOrigin = $result[$i]['to'];

		    $changeCount++;
		    if($changeCount > count($input)) {
			break;
		    }
		}

		sort($result);
		return $result;
	}
}
