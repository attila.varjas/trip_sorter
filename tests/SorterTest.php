<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers Sorter
 */
class SorterTest extends TestCase
{
    /**
     * @var Sorter
     */
    private $sorter;

    /**
     * SetUp.
     */
    public function setUp()
    {
        $this->sorter = new Sorter();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testSort($input, $expected)
    {
        $this->assertEquals($this->sorter->sort($input), $expected);
    }

    /**
     * @dataProvider exceptionDataProvider
     */
    public function testExceptionSort($input)
    {
        $this->expectException(Exception::class);

        $this->sorter->sort($input);
    }

    public function dataProvider()
    {
        return [
            [
                [
                    [
                        'from' => 'B',
                        'to'   => 'C'
                    ],
                    [
                        'from' => 'D',
                        'to'   => 'E'
                    ],
                    [
                        'from' => 'E',
                        'to'   => 'F'
                    ],
                    [
                        'from' => 'A',
                        'to' => 'B'
                    ],
                    [
                        'from' => 'C',
                        'to'   => 'D'
                    ],
                ],
                [
                    [
                        'from' => 'A',
                        'to' => 'B'
                    ],
                    [
                        'from' => 'B',
                        'to'   => 'C'
                    ],
                    [
                        'from' => 'C',
                        'to'   => 'D'
                    ],
                    [
                        'from' => 'D',
                        'to'   => 'E'
                    ],
                    [
                        'from' => 'E',
                        'to'   => 'F'
                    ],
                ],

            ],
            [
                [
                    [
                        'from' => 'B',
                        'to'   => 'C'
                    ],
                ],
                [
                    [
                        'from' => 'B',
                        'to' => 'C'
                    ],
                ],

            ],
        ];
    }

    public function exceptionDataProvider()
    {
        return [
            [
                [
                    [
                        'from' => 'B',
                        'to'   => 'C'
                    ],
                    [
                        'from' => 'D',
                        'to'   => 'E'
                    ],
                    [
                        'from' => 'E',
                        'to'   => 'F'
                    ],
                    [
                        'from' => 'E',
                        'to'   => 'F'
                    ],
                    [
                        'from' => 'A',
                        'to' => 'B'
                    ],
                    [
                        'from' => 'C',
                        'to'   => 'D'
                    ],
                ],
            ],
        ];

    }
}

