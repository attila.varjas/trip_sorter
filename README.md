Trip Sorter - Attila Varjas <attila.varjas@gmail.com>

The solution for the task can be found in the src/Sorter.php file.
Please run composer install to include dependencies.
To test please please run: php phpunit.phar --bootstrap src/Sorter.php tests/
I used PHPUnit 6.1.0 for testing.

For the sake of simplicity I only included the 'from', 'to' attributes of the itinerary elements, adding other attributes such as seat assigments and the type would not change the algorithm. I don't validiate the elements in at the moment that should be done as a next step.
